# cutecom-devel-debian

A Debian Jessie based image with packages for Qt6 development included.


# Instructions

```
$git clone git@gitlab.com:cutecom/cutecom-devel-debian.git
$cd cutecom-devel-debian.git
# make changes

#inspired by https://container-solutions.com/tagging-docker-images-the-right-way/

$git log -1 

$docker build -t cutecom/cutecom-devel-debian:<GIT-HEAD-SHA1-FROM-PREVIOUS-COMMNAD> -t cutecom/cutecom-devel-debian:latest .

$docker login
Login with your Docker ID to push and pull

$docker images

$docker push cutecom/cutecom-devel-debian
```

Head over to https://hub.docker.com/r/cutecom/cutecom-devel-debian/ to verify result

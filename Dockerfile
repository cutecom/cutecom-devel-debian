FROM debian:bullseye-backports
LABEL	maintainer="Meinhard Ritscher <cyc1ingsir@gmail.com>" \
 version="0.6" \
 git-url="https://gitlab.com/cutecom/cutecom-devel-debian"


RUN apt-get update && \
    apt-get install -y \
    build-essential \
    cmake \
    g++-10 \
    git-core \
    libqt6serialport6 \
    libqt6serialport6-dev \
    qt6-base-dev
